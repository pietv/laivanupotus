
const express = require('express');
const bodyParser = require('body-parser');


const app = express();
const port = 3000;
const host = "127.0.0.1";

var cors = require('cors');

app.use(cors());

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");

    if (req.method === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods","PUT,POST,PATCH,DELETE,GET");
        return res.status(200).json({});
    }
    next();
});

app.use(express.static(__dirname+'./client'));

app.get('/', (req, res) => {
    console.log("GET root");
    res.send(randomShip());
});


app.listen(port, host, function() {
    console.log("server up and running @ http://" + host + ":" + port);
});

function randomShip() {
    if (getRandomInt(2) == 0) {
        let aloitus = getRandomInt(15);
        return [aloitus,aloitus+5,aloitus+10];
    } else {
        let aloitus = getRandomInt(3)+5*getRandomInt(4);
        return [aloitus,aloitus+1,aloitus+2];
    }
}


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}


